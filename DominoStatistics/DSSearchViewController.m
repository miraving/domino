//
//  DSSearchTableViewController.m
//  DominoStatistics
//
//  Created by Kateryna Obertynska on 9/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "DSSearchViewController.h"
#import "DSResultsViewController.h"
#import "DSPerson.h"
#import "UUInputAccessoryView.h"
#import <SIAlertView.h>

#define kPersonSaveKey      @"persons1"

@interface DSSearchViewController () <UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *mainListPersons;
@property (nonatomic, strong) NSMutableArray *displayListPersons;
@property (nonatomic, strong) NSMutableArray *searchedListPerson;

@end

@implementation DSSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];

    if (self)
    {
        // Custom initialization
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.searchedListPerson = [NSMutableArray new];
    self.type = kNormalType;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [self loadList];
    [self reLoadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self saveList];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.type == kNormalType) ? [self.displayListPersons count] : [self.searchedListPerson count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"personCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];

    DSPerson *person = (self.type == kNormalType) ? self.displayListPersons[indexPath.row] : self.searchedListPerson[indexPath.row];
    [cell.textLabel setText:person.name];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = cell.textLabel.text;

    DSResultsViewController *vc = [(UINavigationController *) self.parentViewController viewControllers][0];
    BOOL isAdded = [vc setObjectToArray:cellText];

    if (isAdded)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention"
                              message:@"Selected person exists!"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.displayListPersons removeObjectAtIndex:indexPath.row];
        [self.mainListPersons removeObjectAtIndex:indexPath.row];
        
        [self saveList];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
    }
}

#pragma mark - Private API
- (void)reLoadData
{
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    self.mainListPersons = [[self.mainListPersons sortedArrayUsingDescriptors:@[sd]] mutableCopy];
    
    [self excludePersonFromListAtArray:self.existPersonArray];
    [self.tableView reloadData];
}

- (void)excludePersonFromListAtArray:(NSArray *)exArray
{
    self.displayListPersons = [self.mainListPersons mutableCopy];
    
    if ([exArray count] == 0)
        return;
    
    for (DSPerson *exPerson in exArray)
    {
        for (int i = 0; i < [self.displayListPersons count]; i++)
        {
            DSPerson *person = self.displayListPersons[i];
            if ([person.name isEqualToString:exPerson.name])
            {
                [self.displayListPersons removeObject:person];
            }
        }
    }
}

- (void)saveList
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.mainListPersons];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:kPersonSaveKey];
    
    [defaults synchronize];
}

- (void)loadList
{
    NSArray *objects = nil;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id encodedObject = [defaults objectForKey:kPersonSaveKey];
    
//    if ([encodedObject isKindOfClass:[NSArray class]])
//    {
//        NSArray *a = encodedObject;
//        if ([[a lastObject] isKindOfClass:[NSString class]])
//        {
//            SIAlertView *a = [[SIAlertView alloc] initWithTitle:NSLocalizedString(@"ATTENTION_TITLE", nil)
//                                                     andMessage:NSLocalizedString(@"ATTENTION_MGS", nil)];
//            [a addButtonWithTitle:NSLocalizedString(@"BUTTON_OK", nil)
//                             type:(SIAlertViewButtonTypeDestructive)
//                          handler:^(SIAlertView *alertView) {
//                
//                [defaults removeObjectForKey:kPersonSaveKey];
//                [self loadList];
//            }];
//            [a show];
//            
//            return;
//        }
//    }
    
    objects = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];

    if (objects == nil)
    {
        objects = [NSArray new];
    }
    
    self.mainListPersons = [objects mutableCopy];
}


- (IBAction)addNewPerson:(id)sender
{
    [UUInputAccessoryView showKeyboardType:(UIKeyboardTypeDefault) Block:^(NSString *contentStr) {
        
        if ([contentStr length] > 0)
        {
            for (DSPerson *person in self.mainListPersons)
            {
                if ([person.name isEqualToString:contentStr])
                {
                    //__weak typeof(self) bSelf = self;
                    
                    SIAlertView *alert = [[SIAlertView alloc] initWithTitle:NSLocalizedString(@"The same value", nil)
                                                                 andMessage:NSLocalizedString(@"Please change name!", nil)];
                    
                    [alert addButtonWithTitle:NSLocalizedString(@"BUTTON_RENAME", nil)
                                         type:(SIAlertViewButtonTypeDefault)
                                      handler:^(SIAlertView *alertView) {
                                          
                                          [bSelf addNewPerson:nil];
                                      }];
                    [alert show];
                    
                    return;
                }
            }
            
            DSPerson *newPerson = [[DSPerson alloc] init];
            [newPerson setName:contentStr];
            [self.mainListPersons addObject:newPerson];
            
            [self saveList];
            [self reLoadData];
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if ([title isEqualToString:@"Add"])
    {
        UITextField *tf = [alertView textFieldAtIndex:0];
        
        if (tf.text.length > 0)
        {
            BOOL match = NO;
            
            for (int indexObject = 0; indexObject < [self.mainListPersons count]; indexObject++)
            {
                DSPerson *person = self.mainListPersons[indexObject];
                
                if ([person.name isEqualToString:tf.text])
                {
                    match = YES;
                }
            }
            
            if (match == NO)
            {
                DSPerson *nePerson = [[DSPerson alloc] init];
                [nePerson setName:tf.text];
                [self.mainListPersons addObject:nePerson];
            }
            else
            {

            }
            
            NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            self.mainListPersons = [[self.mainListPersons sortedArrayUsingDescriptors:@[sd]] mutableCopy];
            
            [self saveList];
            [self reLoadData];
            
        }
        else
        {
            UIAlertView *alertWarning = [[UIAlertView alloc] initWithTitle:@"Empty Name"
                                                                   message:@"please enter name!"
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil, nil];
            [alertWarning show];
        }
    }
    else if ([title isEqualToString:@"OK"] || [title isEqualToString:@"Rename"])
    {
        [self addNewPerson:nil];
    }
}

#pragma mark - Search bar Delegate
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *searchText = [searchBar.text stringByAppendingString:text];

    if ([text length] == 0)
    {
        searchText = @"";
    }

    if ([searchText length] > 0)
    {
        self.type = kSearchType;

        NSArray *sortArray = [self.displayListPersons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", searchText]];
        [self.searchedListPerson setArray:sortArray];
        [self.tableView reloadData];
    }
    else
    {
        self.type = kNormalType;
        [self.searchedListPerson removeAllObjects];
        [self.tableView reloadData];
    }

    return YES;
}

@end
