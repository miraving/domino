//
//  DSPersonCell.h
//  DominoStatistics
//
//  Created by miraving on 11.06.15.
//  Copyright (c) 2015 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSPersonCell : UITableViewCell

- (void)setProgress:(NSNumber *)progressValue;

@end
