//
//  DSViewController.m
//  DominoStatistics
//
//  Created by miraving on 5/2/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import "DSResultsViewController.h"
#import "DSPerson.h"
#import "UUInputAccessoryView.h"
#import "DSPersonCell.h"
#import "DSSearchViewController.h"
#import "UIView+Constraint.h"



@interface DSResultsViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *personWhoPlay;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentController;

@end

@implementation DSResultsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (BOOL)setObjectToArray:(NSString *)string
{
    if (!self.personWhoPlay)
    {
        self.personWhoPlay = [NSMutableArray new];
    }

    if ([string length] > 0 && ![self isPersonExistAtName:string])
    {
        DSPerson *newPerson = [DSPerson new];
        [newPerson setName:string];
        [self.personWhoPlay addObject:newPerson];

        [self switchSort:self.segmentController];

        return YES;
    }

    return NO;
}

- (BOOL)isPersonExistAtName:(NSString *)name
{
    if ([self.personWhoPlay count] > 0 && [name length] > 0)
    {
        for (DSPerson *person in self.personWhoPlay)
        {
            if ([person.name isEqualToString:name])
            {
                return YES;
            }
        }
    }

    return NO;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self.tableView setHidden:([self.personWhoPlay count] == 0)];
    return [self.personWhoPlay count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"cell";
    DSPersonCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];

    DSPerson *p = self.personWhoPlay[indexPath.row];

    [cell.textLabel setText:p.name];
    [cell.detailTextLabel setText:[p pointString]];
    [cell setProgress:p.point];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.personWhoPlay count] > 1)
    {
        DSPerson *person = self.personWhoPlay[indexPath.row];
        [UUInputAccessoryView showKeyboardType:(UIKeyboardTypeNumberPad) Block:^(NSString *contentStr) {
            
            if ([contentStr length] == 0)
                return;
            
            [person addCountPoint:[contentStr integerValue]];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
            [self switchSort:self.segmentController];
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([self.personWhoPlay count] >= 2)
    {
        tableView.tableHeaderView.hidden = NO;
    }
    else
    {
        tableView.tableHeaderView.hidden = YES;
    }

    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.personWhoPlay.count >= 2)
    {
        return 44;
    }

    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.personWhoPlay.count < 2)
    {
        return nil;
    }
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 60)];

    if (self.personWhoPlay.count >= 2)
    {
        UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [resetBtn setBackgroundColor:[UIColor lightGrayColor]];
        [resetBtn setTitle:NSLocalizedString(@"RESET_GAME", nil) forState:UIControlStateNormal];
        [resetBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [footer addSubview:resetBtn];
        [resetBtn addConstraintFromLeft:10 toRight:10];
        [resetBtn addConstraintFromTop:5 toBottom:5];

        [resetBtn addTarget:self action:@selector(resetAccounts)forControlEvents:UIControlEventTouchUpInside];
    }

    return footer;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self.personWhoPlay removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


#pragma mark - Private
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"personList"])
    {
        DSSearchViewController *searchVC = (DSSearchViewController *)segue.destinationViewController;
        [searchVC setExistPersonArray:self.personWhoPlay];
    }
}

#pragma mark - Action
- (IBAction)switchSort:(UISegmentedControl *)sender
{
    NSMutableArray *mArray = self.personWhoPlay;

    switch (sender.selectedSegmentIndex)
    {
    case 0:
    {
        self.personWhoPlay = [[mArray sortedArrayUsingComparator:^NSComparisonResult (id obj1, id obj2) {

                DSPerson *s1 = obj1;
                DSPerson *s2 = obj2;

                return [s1.name compare:s2.name options:NSCaseInsensitiveSearch];
            }] mutableCopy];
        break;
    }
    default:
    {
        self.personWhoPlay = [[mArray sortedArrayUsingComparator:^NSComparisonResult (id obj1, id obj2) {

                DSPerson *s1 = obj1;
                DSPerson *s2 = obj2;

                return [s2.point compare:s1.point];
            }] mutableCopy];
        break;
    }
    }

    [self.tableView reloadData];
}

- (void)resetAccounts
{
    for (DSPerson *person in self.personWhoPlay)
    {
        [person resetPoint];
    }

    [self.tableView reloadData];
}

- (IBAction)deleteGame:(id)sender
{
    [self.personWhoPlay removeAllObjects];
    [self.tableView reloadData];
}

@end
