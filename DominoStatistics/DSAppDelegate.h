//
//  DSAppDelegate.h
//  DominoStatistics
//
//  Created by miraving on 5/2/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
