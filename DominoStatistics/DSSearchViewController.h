//
//  DSSearchTableViewController.h
//  DominoStatistics
//
//  Created by Kateryna Obertynska on 9/11/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kNormalType = 0,
    kSearchType
} Type;

@interface DSSearchViewController : UITableViewController

@property (nonatomic, assign) Type type;
@property (nonatomic, assign)  id delegate;

@property (nonatomic, strong) NSArray *existPersonArray;

@end
