//
//  DSPerson.h
//  DominoStatistics
//
//  Created by miraving on 06.06.15.
//  Copyright (c) 2015 miraving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSPerson : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *point;

- (void)addCountPoint:(NSInteger)intPoint;
- (void)resetPoint;
- (NSString *)pointString;
@end
