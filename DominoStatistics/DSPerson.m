//
//  DSPerson.m
//  DominoStatistics
//
//  Created by miraving on 06.06.15.
//  Copyright (c) 2015 miraving. All rights reserved.
//

#import "DSPerson.h"

@implementation DSPerson

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        self.name = @"none";
        self.point = [NSNumber numberWithInt:0];
    }

    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if(self)
    {
        //decode properties, other class vars
        self.name = [decoder decodeObjectForKey:@"name"];
        self.point = [NSNumber numberWithInt:0];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.name forKey:@"name"];
//    [encoder encodeObject:self.point forKey:@"point"];
}

- (void)addCountPoint:(NSInteger)intPoint
{
    NSInteger currPoint = [self.point intValue];
    currPoint += intPoint;

    _point = [NSNumber numberWithInteger:currPoint];
}

- (void)resetPoint
{
    _point = [NSNumber numberWithInt:0];
}

- (NSString *)pointString
{
    return [NSString stringWithFormat:@"%@", self.point];
}

@end
