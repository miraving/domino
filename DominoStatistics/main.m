//
//  main.m
//  DominoStatistics
//
//  Created by miraving on 5/2/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DSAppDelegate class]));
    }
}
