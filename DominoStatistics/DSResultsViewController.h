//
//  DSViewController.h
//  DominoStatistics
//
//  Created by miraving on 5/2/14.
//  Copyright (c) 2014 miraving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSResultsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (BOOL)setObjectToArray:(NSString *)string;

@end
