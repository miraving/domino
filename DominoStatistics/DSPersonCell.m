//
//  DSPersonCell.m
//  DominoStatistics
//
//  Created by miraving on 11.06.15.
//  Copyright (c) 2015 miraving. All rights reserved.
//

#import "DSPersonCell.h"
#import "UIView+Constraint.h"

@interface DSPersonCell ()

@property (nonatomic, strong) UIProgressView *progressView;

@end

@implementation DSPersonCell

- (void)awakeFromNib
{
    // Initialization code
    CGRect r = self.contentView.bounds;
    r.origin.y = CGRectGetHeight(self.contentView.frame) - 2;
    self.progressView = [[UIProgressView alloc] initWithFrame:r];
    [self.progressView setProgress:1];
    [self.progressView setProgressTintColor:[UIColor greenColor]];
    [self.contentView insertSubview:self.progressView atIndex:0];
    
    [self.progressView addConstraintFromLeft:0 toRight:0];
    [self.progressView addConstraintFromBottom:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setProgress:(NSNumber *)progressValue
{
    float progressFloat = [progressValue floatValue];
    
    if (progressFloat == 0)
    {
        [self.progressView setProgress:0 animated:YES];
    }
    else
    {
        while (progressFloat > 1)
        {
            progressFloat = progressFloat / 100;
        }
        
        [self.progressView setProgress:progressFloat animated:YES];
    }
}
@end
